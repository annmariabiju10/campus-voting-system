
from django.shortcuts import render, get_object_or_404,redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponseForbidden
from .forms import RegistrationForm
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth import login,logout,authenticate, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from .models import Candidate,ControlVote,Position,Question,Choice
from .forms import ChangeForm
from .models import VotingTimeSlot
from .forms import VotingTimeSlotForm
from datetime import datetime
from .models import Candidate, ControlVote
from .models import Position
from .forms import ImageUploadForm

# Example view to display uploaded files
from django.http import HttpResponse
from .models import Image

def campaign_feed(request):
    posters = Image.objects.all()  # Fetch all posters from the database
    return render(request, 'feed.html', {'posters': posters})



def upload_file(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            print(form)
            form.save()
            return redirect('dashboard')  
    else:
        form = ImageUploadForm()
        return render(request, 'campaign.html', {'form': form})


def homeView(request):
    return render(request, "home.html")

def registrationView(request):

    if request.method == "POST":

        form = RegistrationForm(request.POST)

        if form.is_valid():

            cd = form.cleaned_data

            form.cleaned_data['role']="1"
            if cd['password'] == cd['confirm_password']:

                obj = form.save(commit=False)
                obj.set_password(obj.password)
                obj.save()

                messages.success(request, 'You have been registered.')

                return redirect('home')
            else:

                return render(request, "registration.html", {'form':form,'note':'password must match'})
    else:

        form = RegistrationForm()

    return render(request, "registration.html", {'form':form})

def loginView(request):
    if request.method == "POST":

        usern = request.POST.get('username')
        passw = request.POST.get('password')
        role=request.POST.get('role')

        user = authenticate(request, username=usern, password=passw)
        if user is not None and (int(user.role)>=int(role)):
            login(request,user)
        
            if role =="1":
                return redirect('dashboard')
            else:
                return redirect('campaign') 
        else:

            messages.success(request, 'Invalid username or password!')
            return render(request, "login.html")
    else:

        return render(request, "login.html")



@login_required
def campaignView(request):
    return render(request,"campaign.html")

@login_required
def logoutView(request):
    logout(request)
    return redirect('home')

@login_required
def dashboardView(request):
    return render(request, "dashboard.html")

@login_required
def positionView(request):

    obj = Position.objects.all()

    return render(request, "position.html", {'obj':obj})

@login_required
def candidateView(request, pos):

    current_time = datetime.now()

    active_time_slots = VotingTimeSlot.objects.filter(start_time__lte=current_time, end_time__gte=current_time)
    if not active_time_slots.exists():
        return HttpResponseForbidden("Voting is not currently open.")


    obj = get_object_or_404(Position, pk = pos)

    if request.method == "POST":

        temp = ControlVote.objects.get_or_create(user=request.user, position=obj)[0]

        if temp.status == False:

            temp2 = Candidate.objects.get(pk=request.POST.get(obj.title))
            temp2.total_vote += 1
            temp2.save()
            temp.status = True
            temp.save()
            return HttpResponseRedirect('/position/')
        else:

            messages.success(request, 'you have already been voted this position.')
            return render(request, 'candidate.html', {'obj':obj})
    else:

        return render(request, 'candidate.html', {'obj':obj})

@login_required
def resultView(request):

    obj = Candidate.objects.all().order_by('position','-total_vote')

    return render(request, "result.html", {'obj':obj})

@login_required
def candidateDetailView(request, id):

    obj = get_object_or_404(Candidate, pk=id)

    return render(request, "candidate_detail.html", {'obj':obj})

@login_required
def changePasswordView(request):

    if request.method == "POST":

        form = PasswordChangeForm(user=request.user, data=request.POST)

        if form.is_valid():

            form.save()
            update_session_auth_hash(request,form.user)

            return redirect('dashboard')
    else:

        form = PasswordChangeForm(user=request.user)

    return render(request, "password.html", {'form':form})

@login_required
def editProfileView(request):

    if request.method == "POST":

        form = ChangeForm(request.POST, instance=request.user)

        if form.is_valid():

            form.save()
            return redirect('dashboard')
    else:

        form = ChangeForm(instance=request.user)

    return render(request, "edit_profile.html", {'form':form})

def add_time_slot(request):
    if request.method == 'POST':
        form = VotingTimeSlotForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('view_time_slots')
    else:
        form = VotingTimeSlotForm()
    return render(request, 'add_time_slot.html', {'form': form})

def view_time_slots(request):
    time_slots = VotingTimeSlot.objects.all()
    return render(request, 'view_time_slots.html', {'time_slots': time_slots})

@login_required
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'index.html', context)

@login_required
def detail(request, question_id):
    try:
        question = Question.objects.get(pk = question_id)
    except Question.DoesNotExist:
        raise Http404('Question does not exist')
    return render(request, 'details.html', 
                    {'question': question})

@login_required
def results(request, question_id):
    question = get_object_or_404(Question, pk = question_id)
    return render(request, 'results.html',                  
                    {'question':question})

@login_required
def vote(request, question_id):
    question = get_object_or_404(Question, pk = question_id)
    try:
        selected_choice = question.choice_set.get(pk = request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'details.html', { 'question': question,'error_message': 'You did not select a choice.'})
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('results', args = (question.id,)))