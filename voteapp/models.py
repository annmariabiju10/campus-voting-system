
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class Image(models.Model):
      title = models.CharField(max_length=100)
      file_field = models.FileField(upload_to='images/')

      def __str__(self):
        return self.title
        
class CustomUser(AbstractUser):
    class_field = models.CharField(max_length=100)
    sem_field = models.CharField(max_length=100)
    ROLE_CHOICES = (
        (1, 'Users'),
        (2, 'Candidate'),
    )
    role = models.CharField(max_length=2,choices=ROLE_CHOICES, default=1)
    #REQUIRED_FIELDS = ['class_field', 'sem_field','role']


# ------------------ Online Voting System - Models ------------------ #

# The Position model is used to create the position table
class Position(models.Model):

    # The title field is used to store the position title
    title = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.title

class Candidate(models.Model):

    name = models.CharField(max_length=100)

    total_vote = models.IntegerField(default=0, editable=False)

    position = models.ForeignKey(Position, on_delete=models.CASCADE)

    image = models.ImageField(verbose_name="Candidate Pic", upload_to='images/')

    def __str__(self):
        return "{} - {}".format(self.name, self.position.title)

class ControlVote(models.Model):

    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    position = models.ForeignKey(Position, on_delete=models.CASCADE)

    status = models.BooleanField(default=False)

    def __str__(self):
        return "{} - {} - {}".format(self.user, self.position, self.status)

class VotingTimeSlot(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

class Question(models.Model):
    question_text = models.CharField(max_length = 300)
    pub_date = models.DateTimeField('date published')
    
    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    choice_text = models.CharField(max_length = 200)
    votes = models.IntegerField(default=0)
    
    def __str__(self):
        return self.choice_text
