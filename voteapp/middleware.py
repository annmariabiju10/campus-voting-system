# middleware.py
from datetime import datetime
from django.http import HttpResponseForbidden
from .models import VotingTimeSlot

class VotingTimeSlotMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        current_time = datetime.now()
        active_time_slots = VotingTimeSlot.objects.filter(start_time__lte=current_time, end_time__gte=current_time)
        if not active_time_slots.exists():
            return HttpResponseForbidden("Voting is not currently open.")
        return self.get_response(request)
