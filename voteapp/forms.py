
from django import forms
from django.contrib.auth.models import User
from .models import CustomUser
from .models import VotingTimeSlot
from .models import Image

class ImageUploadForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['title','file_field']


class RegistrationForm(forms.ModelForm):
    confirm_password = forms.CharField(max_length=100, widget=forms.PasswordInput)
    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'email', 'password','class_field', 'sem_field']
        widgets = {
            'password': forms.PasswordInput,
        }

class ChangeForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'email']

class VotingTimeSlotForm(forms.ModelForm):
    class Meta:
        model = VotingTimeSlot
        fields = ['start_time', 'end_time']