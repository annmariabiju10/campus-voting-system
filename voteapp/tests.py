from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User
from .models import Position, Candidate, ControlVote, VotingTimeSlot, Question, Choice
from datetime import datetime
from .models import CustomUser
from django.utils import timezone

class PositionModelTest(TestCase):
    def setUp(self):
        self.position = Position.objects.create(title="President")

    def test_position_title(self):
        self.assertEqual(self.position.title, "President")

class CandidateModelTest(TestCase):
    def setUp(self):
        self.position = Position.objects.create(title="President")
        self.candidate = Candidate.objects.create(name="John Doe", position=self.position)

    def test_candidate_name(self):
        self.assertEqual(self.candidate.name, "John Doe")

class ControlVoteModelTest(TestCase):
    def setUp(self):
        self.position = Position.objects.create(title="President")
        self.user = CustomUser.objects.create(username="testuser", email="test@example.com", class_field="Class X", sem_field="Semester 1", role=1)
        self.control_vote = ControlVote.objects.create(user=self.user, position=self.position)

    def test_control_vote_user(self):
        self.assertEqual(self.control_vote.user, self.user)

class QuestionModelTest(TestCase):
    def setUp(self):
        self.question = Question.objects.create(question_text="What is your favorite color?", pub_date=timezone.now())

    def test_question_text(self):
        self.assertEqual(self.question.question_text, "What is your favorite color?")

class ChoiceModelTest(TestCase):
    def setUp(self):
        self.question = Question.objects.create(question_text="What is your favorite color?", pub_date=timezone.now())
        self.choice = Choice.objects.create(question=self.question, choice_text="Blue")

    def test_choice_text(self):
        self.assertEqual(self.choice.choice_text, "Blue")
